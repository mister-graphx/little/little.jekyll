# Little Jekyll

## Usage

`config/app.yaml`

```yaml
parameters:
    # init plugins
    plugins: ['templateEngine','formatters','assetManager','JekyllPages']

    base_url:        'http://mywebhost.tld'

    app:
        base_url: '%base_url%'
        title: 'My App'
        description: 'App description'



```

## Frontmatters

```yaml
---
title:
description:
robots:
formatter:
layout:        
---

```

## Templates

Twig | Description   
---- | ----
`{{app.title}}` | Titre du site ou applicatif
`{{app.description}}` | Description du site ou applicatif
`{{ meta.title }}` |  utilisé pour le titre de la page et meta title
`{{ meta.description }}` |  description courte de la page et meta Description
