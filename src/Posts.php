<?php
namespace Little\ContentProviders ;
/**
 * Posts content provider
 *
 * Blog Post Content
 *
 * @todo bug quand plusieurs Post porte la meme date.
*/
class Posts extends contentProvider {

	// Liste de tous les contenus (@ \contentProviders)
	protected	$allContents;
	// DataObject content
	protected 	$content;

  protected $blogPath;

  public function init(){

    $this->blogPath = $this->config->get('blog.blog_folder');

    // Get the files list
    $files = $this->getFiles(APP_DIR.$this->blogPath, $this->config->get('app.content_extension'));

		// Build de All Posts Listing
    $posts = $this->filesToContentsList($files, $this->router->currentUri(),'', $path_prefix = APP_DIR.$this->blogPath);

    // Set the template engine Post list
    $this->templateEngine->addGlobal('posts', $this->allContents);

    // Routing
    $blogRoutes = $this->filesToRoutes($files);

    foreach ($blogRoutes as $route){

       $this->router->add('GET', $route['route'], function() use ($route) {

         $this->content->import($this->getFileData(APP_DIR.$this->blogPath.$route['path']));

  	     $template = 'posts';

  			 if ( $this->content->get('meta.layout') == TRUE ) {
  				  $template = $this->content->get('meta.layout');
  			 }

  			 $this->renderContent($template, $this->content->export());

			 });
    }
  }
          
}
