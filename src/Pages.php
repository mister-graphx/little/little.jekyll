<?php namespace Little\Jekyll ;

use Little\Kore\Utils\UtilitiesFramework as Utils;

/**
 * Pages Contents
 *
*/
class Pages extends Jekyll {

    // Liste de tous les contenus (@ \contentProviders)
    protected $allContents;
    // Content Data Object
    // @use dotNotation
    protected $content;

    protected $files;
    protected $pagesListing;

    public function twigGlobals(){
        // Template engine custom globals and functions
        $this->templateEngine->addGlobal('pages', $this->allContents);
        $this->templateEngine->addFunction('pages_nav', function () {
            echo $this->pages_nav();
        }, array('is_safe' => array('html')));
    }

    public function onLoad() {
        $path = $this->config->get('pages_path');

        if(!is_dir($path)){
            die("Directory $path not exist");
        }
        // Recupère la liste de tout les contenus
        $files = $this->getFiles($path, $this->config->get('content_extension'));
        $this->files = $files;
        // Construit la navigation
        $pagesListing = $this->filesToContentsList($this->files, $this->router->currentUri(), '', $path_prefix = $path);
        $this->pagesListing = $pagesListing;
    }

    public function setUpRoutes() {
        // Route Set UP
        $pagesRoutes = $this->filesToRoutes($this->files);
        $files = $this->files;
        $pagesListing = $this->pagesListing;

        foreach ($pagesRoutes as $route) {
            $this->router->add('GET', $route['route'], function () use ($route, $files, $pagesListing) {
                $path = $this->config->get('pages_path');
                $current = $path.$route['path'];
                // Recupere le content et headers
                // et ajoute a l'obj content
                $this->content->import($this->getFileData($current));
                $fileInfos = $this->getFileInfos($current, $route);
                // Recupere les informations du fichier et met a jour les datas
                $this->content->import($fileInfos);
                // Metas traitements
                // Excerpt
                $excerpt = '';
                if ($this->content->get('content') == true && $this->content->get('meta.excerpt') === null) {
                    $excerpt = $this->formatter->format($this->content->get('content'));
                    $stringUtil = Utils::Factory('StringUtils');
                    $excerpt = $stringUtil->excerpt($excerpt, $this->config);
                } else {
                    $excerpt = $this->content->get('meta.excerpt').$this->config->get('excerpt.end');
                }
                $this->content->set('meta.excerpt', $excerpt);

                $template = 'page';

                if ($this->router->currentUri() == '') {
                    $this->content->set('is_front_page', true);
                    $template = 'index';
                }

                // Rendu du contenu : Applique le formatter, rendu twig
                $this->renderContent($template, $this->content->export());
            });

        }

    }
  /**
    * Pages Nav
    *
    * Construit les données pour la function Twig
    * qui affiche la navigation
    *
    */
    public function pages_nav() {
        $filesList = $this->pagesListing;
        $utils =  Utils::Factory('Html');
        $html = $utils->navToHtml($filesList, $this->config);
        return $html;
    }
}
