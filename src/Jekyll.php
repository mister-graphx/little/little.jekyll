<?php
namespace Little\Jekyll ;

use Dflydev\DotAccessData\Data;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;
use Little\Kore\Utils\UtilitiesFramework as Utils;
/*
 * Jekyill like blog engine for Little
 */
class Jekyll {
    protected $config;
    /** @var \Symfony\Component\Yaml\Parser $Yaml               Yaml Object */
    protected $Yaml;
    // Active Formatter
    protected $formatter;
    // Availables Formatters list
    protected $formatters;

    protected $router;
    protected $events;
    protected $templateEngine;

    protected $allContents; // All Contents
    /** @var \DotAccessData\Data templateEngine datas (meta,content) */
    protected $content; // Content Data Object @test


    public function __construct($config,$events, $router, $formatters, $templateEngine) {

        $this->formatter = $formatters->formatter;
        $this->formatters = $formatters;
        $this->router = $router;
        $this->events = $events;
        $this->Yaml = new Parser();
        $this->templateEngine = $templateEngine;

        $this->config = new Data($config);
        $this->content = new Data(array());


        // $this->getDataFile($this->router->currentUri());
        //
        $this->events->addListener('Kore.loaded',function($event,$container){
            $container->get('events')->emit('Jekyll.loaded',$this->config);
            $this->onLoad();
        });

        $this->events->addListener('Twig.beforeGlobals',[$this, 'twigGlobals']);

        $this->events->addListener('Kore.afterSetupRoutes',[$this, 'setUpRoutes']);

        $this->events->addListener('Kore.error404',function($event,$message=false){
            $finder = new Finder();
            $finder->files()->in([ $_ENV{'PUBLIC_PATH'} , __DIR__.'/public/'])->name('404.md');
            foreach ($finder as $file) { break; }
            $datas = $this->frontmatterExtract($file->getContents());
            $datas['error'] = $message;
            $this->renderContent('404',$datas);
        });

    }
  /**
    * Kore.loaded additions for Jekyll Content providers
    *
    */
    public function onLoad() {}
  /**
    * Twig.beforeGlobals templates engine Globals, users functions
    *
    */
    public function twigGlobals() {}
  /**
    * Kore.setUpRoutes additions for Jekyll Content providers
    *
    */
    public function setUpRoutes() {}
  /**
    * search a DataFile according to the file route
    *
    * @param \Router\route \$parameter [description]
    */
    public function getDataFile($route) {
        $datas = new DataFiles($route, $this->templateEngine);
        return $datas;
    }
  /**
    * Get the files
    *
    * Build a recursive file listing of the dir, according to ext
    * return an array with nice and raw file name, directory are indexes
    *
    *
    * @param string	$dir	the directory path
    * @param string 	$ext 	extension we search for
    * @param bool	    $top	internal value for top ul
    */
    public function getFiles($dir, $extension, $top = true) {
        (is_dir($dir)) ? $dir = rtrim($dir, '/') : die();
        $result = array();
        $dirs = array();
        $files = array();
        $sdir = scandir($dir);
        foreach ($sdir as $key => $value) {
            if (!in_array($value, array('.','..'))) {
                $ext = pathinfo($value, PATHINFO_EXTENSION);
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $dirs[$value] = $this->getFiles($dir . DIRECTORY_SEPARATOR . $value, $extension, false);
                } elseif ('.' . $ext == $extension) {
                    if (preg_match('/^\d+\-/', $value)) {
                        list($index, $path) = explode('-', $value, 2);
                        $files[$index] = array(
                            'nice' => $path,
                            'raw' => $value
                        );
                    } else {
                        $files[] = array(
                            'nice' => $value,
                            'raw' => $value
                        );
                    }
                }
            }
        }
        ksort($dirs);
        ksort($files);

        if ($top) {
            $result = array_merge($files, $dirs);
        } else {
            $result = array_merge($dirs, $files);
        }
        return $result;
    }
  /**
    * files to route
    *
    * build all routes with $files
    *
    * @param array	$files
    * @param string	$route_prefix
    * @param string	$path_prefix
    * @return array	return a flat array item =>(path|route)
    */
    public function filesToRoutes($files, $route_prefix = '', $path_prefix = '') {
        $result = array();

        foreach ($files as $key => $value) {
            if (!is_int($key)) {
                if (preg_match('/^\d+\-/', $key)) {
                    list($index, $path) = explode('-', $key, 2);
                    $result = array_merge($result, $this->filesToRoutes($value, $route_prefix . $path . '/', $path_prefix . $key . '/'));
                } else {
                    $result = array_merge($result, $this->filesToRoutes($value, $route_prefix . $key . '/', $path_prefix . $key . '/'));
                }
            } else {
                $route = str_replace($this->config->get('content_extension'), '', $value['nice']);
                if ($route == 'index') {
                    $route = '';
                }

                $result[] = array(
                    'route' => $route_prefix . $route,
                    'path' => $path_prefix . $value['raw']
                );
            }
        }
        return $result;
    }
  /**
    * Files to contents lists
    *
    * return a list of contents, format the content datas
    * set the content list accessible in the template engine
    *
    */
    public function filesToContentsList($files, $currentUri, $route_prefix = '', $path_prefix = '') {
        $result = array();
        $content_path = $this->config->get('pages_path');

        foreach ($files as $key => $value) {
            if (!is_int($key)) {
                if (preg_match('/^\d+\-/', $key)) {
                    list($index, $path) = explode('-', $key, 2);
                    $result[$key] = $this->filesToContentsList($value, $currentUri, $route_prefix . $path . '/', $path_prefix . $key . '/');
                } else {
                    $result[$key]['path'] = $route_prefix . $key . '/' ;
                    $result[$key]['_childs'] = $this->filesToContentsList($value, $currentUri, $route_prefix . $key . '/', $path_prefix . $key . '/');
                    // if an index file is in path/route
                    // Set meta title as directory title
                    if(file_exists($content_path.$route_prefix . $key . '/index.md')
                      && $content = $this->getFileData($content_path.$route_prefix . $key . '/index.md')
                    ){
                        $result[$key]['title'] = $content['meta']['title'];
                    }else{
                        $result[$key]['title'] = $key;
                        // var_dump(APP_DIR.PAGES_DIR.$route_prefix . $key . '/index.md');
                    }
                }
            } else {
                $route = str_replace($this->config->get('content_extension'), '', $value['nice']);

                if ($route == 'index') {
                    $route='/';
                }

                if ($route == '') {
                    $route = '/';
                }

                if (!$currentUri) {
                    $currentUri = '/';
                }

                $data = $this->getFileData($path_prefix . $value['raw']);

                $title = isset($data['meta']['title']) ? $data['meta']['title'] : '';
                if (!$title) {
                    $title = ucwords(str_replace(array('-', '_'), ' ', basename($route)));
                }

                $active = false;
                if ($route_prefix . $route === $currentUri) {
                    $active = true;
                }

                $excerpt = '';
                if (isset($data['content']) && !isset($data['meta']['excerpt'])) {
                    $excerpt = $this->formatter->format($data['content']);
                    $stringUtil = Utils::Factory('StringUtils');
                    $excerpt = $stringUtil->excerpt($excerpt, $this->config);
                } else {
                    $excerpt = $data['meta']['excerpt'].$this->config->get('excerpt.end');
                }
                // Date
                setlocale(LC_TIME, "fr_FR");
                $date_format = $this->config->get('date_format');

                $published = strftime($date_format,time());

                // ? Only for blog posts
                if (preg_match('/^\d+\-/', $value['raw'])) {
                    list($time, $path) = explode('-', $value['raw'], 2);
                    $published = strftime($date_format, strtotime($time));
                }

                if (isset($data['meta']['published'])) {
                    $published = strftime($date_format, strtotime($data['meta']['published']));
                }

                if (!isset($data['meta']['exclude_from_nav']) || !$data['meta']['exclude_from_nav']) {
                    $contentInfos = array(
                            'title'  => $title,
                            'description'=> isset($data['meta']['description']) ? $data['meta']['description'] : '' ,
                            'excerpt'=>$excerpt,
                            'file'=> $value['raw'],
                            'published'=>$published,
                            'url'    => $route_prefix . $route,
                            'active' => $active
                        );
                    $result[] = $contentInfos;
                    if (!($value['raw']=='index'.$this->config->get('content_extension'))) {
                        $this->allContents[] = $contentInfos;
                    }
                }
            }
        }
        return $result;
    }

  /**
    * return meta and content
    *
    * @param string $route_path file/path
    * @return array $data content|metas
    */
    public function getFileData($route_path) {
        $data = null;
        if (file_exists($route_path)) {
            // Parse the file extract content and metas
            $data = $this->frontmatterExtract(file_get_contents($route_path));
        }

        return $data;
    }
  /**
    * Get file infos
    *
    * @param string $file_route
    * @param string $route
    *
    */
    public function getFileInfos($file_route, $route) {
        $fileInfos = array();
        $date_format = $this->config->get('date_format');
        $file = new \SplFileInfo($file_route);

        if (file_exists($file_route)) {
            $fileInfos['file'] = $file->getBasename();
            $fileInfos['absolute_path'] = $file->getPath();
            $fileInfos['last_modified'] = strftime($date_format, $file->getMTime());
            $fileInfos['route'] = $route;

            // Publication
            // @todo gerer la date de publication
            $published = strftime($date_format);
            if (preg_match('/^\d+\-/', $fileInfos['file'])) {
                list($time, $path) = explode('-', $fileInfos['file'], 2);
                $published = strftime($date_format, strtotime($time));
            }
            if (isset($data['meta']['published'])) {
            	$published = strftime($date_format, strtotime($data['meta']['published']));
            }
            $fileInfos['meta']['published'] = $published;
        }

        return $fileInfos;
    }

    public function set_metas($data) {
        return $data;
    }
  /**
    * renderContent
    *
    * Twig render method for the current Content Object route/page
    *
    * @param $template
    * @param $datas
    *
    */
    public function renderContent($template, $datas) {
        // Formatter set from Frontmatter
        if (isset($datas['meta']['formatter']) && !empty($datas['meta']['formatter'])) {
            $this->formatter = $this->formatters->getFormatter($this->content->get('meta.formatter'));
        }

        // format content
        $datas['content'] = $this->formatter->formatContent($datas['content'], $datas['meta']);

        // Template/ Layout set from Frontmatter
        if (isset($datas['meta']['layout']) && !empty($datas['meta']['layout'])) {
            $template = $datas['meta']['layout'];
        }

        return $this->templateEngine->render($template, $datas);
    }
  /**
    * parse the file content
    * extract frontmatter/content parts
    *
    * @param string $file_contents file content
    * @return array|string $data return an array (meta|content)
	*/
	public function frontmatterExtract($file_contents){

        if (preg_match_all('/^[-]{3}.(.*?)[-]{3}/ims', $file_contents, $match) && isset($match[1]) && isset($match[1][0])) {
            $headers = $match[1][0];
        }

		if (isset($headers)) {
			try {
				$data['meta'] = $this->Yaml->parse($headers);
			} catch(\Exception $e) {
				echo $e->getMessage();
			}
		}

		$data['content'] = $this->remove_frontmatter($file_contents);

		if (!isset($data['content'])) {
			$data['content'] = $file_contents;
		}

		return $data;
	}
  /**
    * Remove frontmatter section
    *
    * @param string $string file content
    * @return string
	*/
    public function remove_frontmatter($string){
		$string = preg_replace('#[-]{3}.+?[-]{3}#s', '', $string, 1);
		return $string;
	}

}
