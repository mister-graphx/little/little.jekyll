<?php

namespace Little\ContentProviders ;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

/**
 * DataFiles
 *
 * add Globals.Data to the template engine,
 * if a csv, yaml, json file is found with the same route in the data folder
 *
 * @todo Ajouter un convertisseur de format Excel : <https://github.com/faisalman/simple-excel-php>
*/
class DataFiles {

  private $finder;
  private $templateEngine;
  private $data_folder;
  private $data_file;

  public function __construct($route, $templateEngine){
    $this->data_folder = APP_DIR.DATAS_DIR;
    $this->finder = new Finder();
    $this->templateEngine = $templateEngine;

    $this->getDataFile($route);
  	/** Twig functions & Filters */
  	$this->addDataFilters();
  	$this->addDataFunctions();
  }

/**
  * Get Data from File
  *
  * @param string $file absolute path to the file
  * @state dev, untested
	*/
	public function getDataFromFile($file){
		$datas = $this->getDataFile($file);
		return $datas;
	}

 /**
	 * Get Data File
	 *
   * Search for a dataFile in data Folder
   * Add Globals :
   * accessible in content and layout in the data namespace
   *
   * @todo prevoir le cas ou plusieurs fichiers sont trouvés
   *
   * @param string $path absolute path to the dataFile
   * @return void add Datas to Twig context
   */
    protected function getDataFile($path){

  		/** Is it index route */
      if($path == '' || preg_match('#\/$#i', $path)){
          $file['name'] = 'index';
          $file['path'] = $path;
      }
  		else{
          preg_match('#[\w\d\-\_\.]+$#i', $path, $match);
          $file['name'] = $match[0];
          $file['path'] = preg_replace('#'.$file['name'].'#','',$path,1);
      }

      if(is_dir($this->data_folder.$file['path'])){

           $result = [];
           $files = $this->finder->files()
                                ->in($this->data_folder.$file['path'])
                                ->depth('== 0')
                                ->name($file['name'].'.{yaml,json,csv}');

          if(iterator_count($files) == 1 ){
            foreach ($files as $dataFile){
                $result['datas'] = $dataFile->getContents();
                $result['real_path'] = $dataFile->getRealpath();
                $result['type'] = $dataFile->getExtension();
                $result['namespace'] = $file['name'];
            }

			      $datas = $this->decode($result);

			      $this->templateEngine->addGlobal($result['namespace'],$datas);
          }
        }
    }
 /**
	 * Decode
	 *
	 * @see http://php.net/manual/fr/function.json-last-error.php
	 * @todo  retour d erreur sur un json incorrect
	 * @param array $dataFile (type, datas, real_path, namespace)
	 *
	*/
    protected function decode($dataFile){

        if(is_array($dataFile)){
            switch ($dataFile['type']) {
                case 'yaml':
                    $dataFile['datas'] = Yaml::parse($dataFile['datas']);
                break;
                case 'json':
						$dataFile['datas'] = json_decode($dataFile['datas']);
						switch (json_last_error()){
							case JSON_ERROR_SYNTAX:
								echo ' - Erreur de syntaxe ; JSON malformé';
							break;
						};
                break;
                case 'csv':
                    $dataFile['datas'] = $this->read_csv($dataFile['real_path']);
                break;
                default:
                   throw new \Exception('Invalid Extension, file must be yaml, json or csv');
            }

            return $dataFile;
        }
    }

    /*
     * function read_csv
     *
     * @param $file absolute file path
     * @return array $datas
     */
    protected function read_csv($file, $separator = ",") {
        $rows = array();
        $headers = array();
        if (file_exists($file) && is_readable($file)) {
            $handle = fopen($file, 'r');
            while (!feof($handle)) {
                $row = fgetcsv($handle, 10240, $separator, '"');
                if (empty($headers))
                    $headers = $row;
                else if (is_array($row)) {
                    array_splice($row, count($headers));
                    $rows[] = array_combine($headers, $row);
                }
            }
            fclose($handle);
        } else {
            throw new \Exception($file . ' doesn`t exist or is not readable.');
        }
        return $rows;
    }

  // Twig Filters
  protected function addDataFilters(){
    // $this->templateEngine->addFilter('dump',function($string){
  	// 	if(is_array($string) )
  	// 		var_dump($string);
  	// 	else
  	// 		return 'Is string: '.$string;
  	// }, array());
  }

	// Twig Function
	protected function addDataFunctions(){
		/** Twig function getDatas(namespace = "", file = '')
		* @param namespace
		* @param file
		*/
		$this->templateEngine->addFunction('getDatas', function($context, $namespace, $file){

        $datas = [];
				$files = $this->finder
							->name($file.'.{yaml,json,csv}')
							->in($this->data_folder);

				if(iterator_count($files) == 1 ){
					foreach($files as $file){
						$datas['datas'] = $file->getContents();
						$datas['real_path']= $file->getRealPath();
						$datas['type']= $file->getExtension();
						$datas['namespace']= $namespace;
					}

					$datas = $this->decode($datas);

					return $datas['datas'];
				}
			},
			array('needs_context' => true)
		);

	}







}
